class Dog{
    public void bark(){
        System.out.println("woof woof");
    }
}
class Boxer extends Dog{
    public void sniff(){
        System.out.println("sniff ");
    }
    //Overriding
    public void bark(){
        System.out.println("Grrr");
    }
}
 
public class start{
    public static void main(String [] args){
        Dog dog = new Boxer();
        dog.bark();
    }
}